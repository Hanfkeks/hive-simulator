import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class GameStatus implements java.io.Serializable{
	private ArrayList<Hive> playerList;
	private int curPlayerIndex;
	private Hive curHive = null;
	private int day = 1;
	private boolean playerFinished = false;
	private int hivenumber = 1;
	
	
	public GameStatus(ArrayList<Hive> plList, int index, Hive curPl, int d, boolean finished,int number){
		playerList=plList;
		curPlayerIndex=index;
		curHive=curPl;
		day=d;
		playerFinished=finished;
		hivenumber=number;
		System.out.println("Gamestatus written on memory, proceeding to write on Disk");
		
	}
	
	public ArrayList<Hive> getPlayerList(){
		return playerList;
	}
	
	public int getCurPlayerIndex(){
		return curPlayerIndex;
	}
	
	public Hive getCurHive(){
		return curHive;
	}
	
	public int getDay(){
		return day;
	}
	
	public boolean getPlayerFinished(){
		return playerFinished;
	}
	public int getHivenumber(){
		return hivenumber;
	}

}
