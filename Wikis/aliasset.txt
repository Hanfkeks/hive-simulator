Wikipage of Advance
Usage: "aliasset [alias] {[line];[line];...[line]}"
See also "alias"

Aliases are just short forms of actual commands with parameters.
To define a alias you can later use, type in "aliasset" and the name of your future alias.
Then type in the command you want to execute.
You can even type in multiple commands, just seperate them with a semicolon (;).
Just experiment a little bit to learn how to use it.

Here's an example:
   "aliasset work harvest worker yellow_dandelion;craft crafter honey;status"
   Given that the bees "worker" and "crafter", and the plant "yellow_dandelion" exist, you can simply harvest and craft honey by typing "work"
   Note that it does not matter if these commands work or not, the game will just try to execute them, if its not possible it will show you
     the corresponding message. E.g. "worker is not available"