import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Vector;

public class CommandPromtManager {

	// Attributes
	private ArrayList<Hive> playerList;
	private int curPlayerIndex;
	private Hive curHive = null;
	Scanner scanner;
	private String scanbuf = ""; // immer die letzte zeile oder letzter
									// relevanter part der Zeile
	private int day = 1;
	boolean playerFinished = false;
	private int hivenumber = 1;
	private int recursiveRuns=0;		//wird nicht gespeichert. sorgt daf�r, dass wenn sich aliases selbst aufrufen nach 20 aufrufen schluss ist

	// Construct0rs:

	public CommandPromtManager(String path, Scanner s) { // only used when
															// loading a game
		scanner = s;
		load(path);
		System.out.println("Savegame Loaded sucessfully");
		gameloop(true);
	}

	public CommandPromtManager(int playercount, Scanner s) {
		// System.out.println("Working Directory: "+System.getProperty("user.dir")+java.io.File.separatorChar+Wikimanager.workingdir());
		scanner = s;
		playerList = new ArrayList<Hive>();
		curPlayerIndex = 0;
		int i = 0;
		scanbuf = readline(); // brauch ich hier weil er die alte zeile sonst
								// glei verwertet
		while (i < playercount) {
			System.out.println("Player " + (i + 1) + ", enter your name:");
			scanbuf = readline();

			if (scanbuf.equals("")) {
				System.out.println("Empty names are not accepted...");
				continue; // ja des lass ma garned erst zu, leere namen und so
			}
			if (scanbuf.contains(" ")) {
				System.out
						.println("Names are only accepted without spaces, otherwhise they won't be selectable");
				continue; // mhm, sowas is ned schoen
			}
			playerList.add(new Hive(3, 3, scanbuf));
			i++;
		}
		System.out
				.println("All players have been set up. You'll start up with a 3x3 Hive");
		System.out.println("If you need help, type \"help\" or \"commands\"");
		gameloop(false);
		System.out.println("Game Over, your Hive survived for " + (day - 1)
				+ " days");

	}
	
	
	public void gameloop(boolean gameLoaded) {
		boolean loaded = gameLoaded;
		while (!playerList.isEmpty()) { // es sind noch Spieler am leben...
			// Tagesloop
			if (!loaded) { // leider gibts ja kein goto
				System.out.println("A new morning dawns");
				System.out.println("Day " + day);
			}
			while (curPlayerIndex < playerList.size()) {
				curHive = playerList.get(curPlayerIndex);
				// remove Player who died in the last round
				if (curHive.getQueen() == null) {
					System.out
							.println("A hive got desintegrated, because the queen is dead!");
					playerList.remove(curPlayerIndex);
					continue;
				}
				if (!loaded){
					curHive.updateEveryone();
				}
				if (curHive.getQueen() != null) {
					System.out.println(curHive.getQueen()
							+ ", it is your turn!");
					while (!playerFinished) { // solange er ned den day beendet:
						loaded = false;// falls geladen wurde soll in zukunft
										// wieder angezeigt werden wenn ein
										// neuer tag beginnt
						System.out.println("_____________________________");
						System.out.printf(":>");
						scanbuf = readline();
						// System.out.println(scanbuf+"=scanbuf"); //zum
						// debuggen
						interpretCommand(scanbuf);

					}
				}
				
				playerFinished = false;
				curPlayerIndex++;
			}
			for(Hive anyplayer:playerList){
				anyplayer.endday();
			}
			curPlayerIndex = 0;
			day++;
		}
	}

	public void interpretCommand(String cmd) {
		String[] vector = cmd.split("\\s");
		// printarray(vector); //zum debuggen
		String[] params;
		switch (vector[0].toLowerCase()) {
		case "exit":
		case "quit":
			System.out.println("Are you sure you want to quit? y/n?");
			boolean answer = false;
			String input = "";
			while (!answer) {
				input = readline();
				if (input.equals("yes") || input.equals("y")) {
					System.exit(0);
				} else if (input.equals("no") || input.equals("n")) {

				}
			}
			System.exit(0);
			break;
		case "nextday":
		case "newday":
			playerFinished = true;
			break;
		case "rename":
			if (vector.length == 3) {
				Bee bufbee = curHive.getBee(vector[2]);
				if (bufbee == null) {
					curHive.renameBee(vector[1], vector[2]);
				} else
					System.out.println("The name " + bufbee
							+ " is already taken");
			} else {
				System.out
						.println("Correct use of \"rename\" is: rename [BeeName] [NewBeeName]");
			}
			break;
		case "save":
			if (vector.length != 1) {
				params = cmd.split(" ", 2);
				save(params[1]);
			} else
				System.out
						.println("You'll need to set a name for your savegame");
			break;
		case "engage":
		case "attack":
			if (vector.length >= 3) {
				Hive bufhive = curHive.getHostile(vector[1]);
				LinkedList<Bee> attackers = new LinkedList<Bee>();
				if (bufhive != null) {
					int i = 0;
					while (i < (vector.length - 2)) {
						attackers.add(curHive.getBee(vector[2 + i]));
						if (attackers.get(i) != null
								&& !(attackers.get(i) instanceof Larva)) {
							if (attackers.get(i).isAvailable()) {

							} else {
								System.out.println(vector[i]
										+ " is not available. Attack cancled");
								return;
							}
						} else {
							System.out.println(vector[i]
									+ "is not a Bee. Attack cancled");
							return;
						}
						i++;
					}
					LinkedList<Bee> defenders = bufhive.getAllBees();
					battle(attackers, defenders, bufhive);
					for (Bee x : attackers) {
						x.decreaseAvailable();
					}
				} else {
					System.out.println("There is no hostile Hive called "
							+ vector[1]);
				}
			} else {
				System.out
						.println("Correct use of \"engage\" is: engage [EnemyHive] {[Bee]...[Bee]}");

			}
			break;
		case "spy":
			if (vector.length == 3) {
				Drone bufdrone = curHive.getDrone(vector[1]);
				if (bufdrone != null) {
					if (bufdrone.isAvailable()) {
						Hive bufhive = curHive.getHostile(vector[2]);
						if (bufhive != null) {
							printarray(bufhive.getStats());
							bufdrone.decreaseAvailable();
						} else {
							bufhive = curHive.getEmptyHive(vector[2]);
							if (bufhive != null) {
								System.out.println("The Layout of "
										+ bufhive.getName()
										+ " is the following:");
								printarray(bufhive.getCombstats());
								bufdrone.decreaseAvailable();
							} else
								System.out
										.println(vector[2] + " is not a Hive");
						}
					} else
						System.out.println(bufdrone + " is not available");
				} else
					System.out.println("404: Drone not found");
			} else
				System.out
						.println("Correct use of \"spy\" is: spy [drone] [hive]");
			break;
		case "smartnext":
			if (!curHive.hasAvailableWorker()) {
				playerFinished = true;
			}
			break;
		case "move":
			if (vector.length == 2) {
				Hive bufhive = curHive.getEmptyHive(vector[1]);
				if (bufhive != null) {
					if (curHive.allAvailable()) {
						if (!curHive.hasLarva()) {
							if (curHive.consumeHoney(15)) {
								curHive.moveToHive(bufhive);
								curHive = bufhive;
								playerList.set(curPlayerIndex, bufhive);
								playerFinished = true;
								System.out
										.println("Your bees now live in a new home");
							} else
								System.out
										.println("Moving your Hive will cost you 15 honey, you only have "
												+ curHive.getHoney());
						} else
							System.out
									.println("Not all of your Bees are available. Moving must be the first action you do");
					} else
						System.out
								.println("You've still got Larvae. You shall not leave your brood");
				} else
					System.out.println(vector[1] + " is not a empty Hive");

			} else
				System.out
						.println("Correct use of \"move\" is: move [EmptyHive]");
			break;
		case "status":
		case "stats":
			params = cmd.split(" ", 4);
			if (params.length <= 1) {
				System.out.println("Overall summary of "
						+ curHive.getQueen().getName() + "'s Hive");
				printarray(curHive.getCombstats());
				printarray(curHive.getStats());
			} else
				switch (vector[1].toLowerCase()) {
				case "hive":
					printarray(curHive.getStats());
					break;
				case "bee":
				case "bees":
					if (params.length == 3) {
						Bee bufbee = curHive.getBee(vector[2]);
						if (bufbee != null) {
							printarray(bufbee.giveDetails());
						}
					}
					break;
				case "wax":
					System.out.println("Wax: " + curHive.getWax() + "/"
							+ curHive.getMaxRessources());
					break;
				case "honey":
					System.out.println("Honey: " + curHive.getHoney() + "/"
							+ curHive.getMaxRessources());
					break;
				case "pollen":
					System.out.println("Pollen: " + curHive.getPollen() + "/"
							+ curHive.getMaxRessources());
					break;
				case "combs":
				case "comb":
					printarray(curHive.getCombstats());
					break;
				case "plant":
				case "plants":
					if (params.length == 3) {
						Plant bufplant = curHive.getPlant(vector[2]);
						if (bufplant != null) {
							printarray(bufplant.giveDetails());
						}
					}
					break;
				default:
					System.out.println("Cannot give the status of " + vector[1]
							+ ", try status bee/plant " + vector[1]);
				}
			break;
		case "harvest":
		case "carry":
			if (vector.length == 3) {
				Worker bufbee = curHive.getWorker(vector[1]);
				if (bufbee != null) {
					Plant bufplant = curHive.getPlant(vector[2]);
					if (bufplant != null) {
						curHive.harvest(bufbee, bufplant);
					} else
						System.out.println(vector[2] + " does not exist");
				} else
					System.out.println(vector[1] + " is not a Worker");
			} else
				System.out
						.println("Correct use of \"harvest\" is: harvest [Bee] [Plant]");
			break;
		case "help":
			if (vector.length != 2) {
				System.out
						.println("The \"help\" command can give you huge portions of information");
				System.out
						.println("It will load up wikipages for any command when paired with it, like: \"help rename\" will load the wikipage of the rename command");
				System.out
						.println("However there is even more to it. Most importantly the tutorial page. Just type in help tutorial");
				System.out
						.println("There are also pages about \"Bee\",\"Worker\",\"Drone\",\"Larva\",\"Queen\" and \"Plant\"");
			} else {
				printarray(Wikimanager.loadWikipage(vector[1]));
			}
			break;
		case "breed":
		case "brood":
			if (vector.length == 2) {
				if (curHive.getQueen() != null) {
					curHive.breedDrone(vector[1]);
				} else {
					System.out.println("A drone needs a Queen to reproduce");
				}
			}
			break;
		case "scout":
			if (vector.length == 2) {
				Worker bufwork = curHive.getWorker(vector[1]);
				if (bufwork != null) {
					if (bufwork.isAvailable()) {
						bufwork.decreaseAvailable();
						// random w�rfeln und evtl Feindlichen oder leeren
						// Hive
						// in die hivelist nehmen
						// einfach einen aus der playerlist rausw�rfeln und
						// wenns der eigene oder bekannter is einen neuen leeren
						// finden
						int rand = (int) (Math.random() * 5);
						if (rand == 4) {
							Hive randhive = playerList
									.get((int) (Math.random() * playerList
											.size()));// zuf�lligen player
														// krallen
							if (randhive == null) {
								System.out.println(bufwork
										+ " saw some magnificant scenery");
							}
							if (randhive != null && randhive != curHive
									&& !(curHive.alreadyDiscovered(randhive))) {
								curHive.discoverHive(randhive);
								System.out.println(bufwork
										+ " has discovered a Hive: "
										+ randhive.getName());
							} else {
								curHive.discoverHive(new Hive(curHive
										.nextHiveLimits(), curHive
										.nextHiveLimits(), hivenumber));
								hivenumber++;
								System.out
										.println(bufwork
												+ " has discovered a new place to settle");
							}
						} else {
							Plant p = PlantFactory.growRandomPlant();
							curHive.findPlant(p);
							System.out.println(bufwork + " has discovered a "
									+ p);
						}
					} else
						System.out.println(bufwork.name + " is not available");
				} else
					System.out.println("404: Worker not found");
			} else
				System.out.println("Correct use of \"scout\" is: scout [Bee]");
			break;
		case "build":
		case "construct":
			if (vector.length > 3) {
				int i = 0;
				boolean fail = false;
				Worker bufwork;
				Worker[] workarray = new Worker[vector.length - 3];
				while (vector.length > (i + 3)) {
					bufwork = curHive.getWorker(vector[3 + i]);
					if (bufwork != null) {
						workarray[i] = bufwork;
					} else {
						System.out.println("Worker: " + vector[3 + i]
								+ " does not exist");
						fail = true;
						return;
					}
					i++;
				}
				if (fail) {
					System.out.println("Building failed");
					break;
				} else {
					int one = 0;
					int two = 0;
					try {
						one = Integer.parseInt(vector[1]);
						two = Integer.parseInt(vector[2]);
					} catch (NumberFormatException e) {
						System.out
								.println("Coordinates are not formated as a number");
						break;
					}
					curHive.buildCombs(two - 1, one - 1, workarray);
				}
			} else
				System.out
						.println("Correct use of \"build\" is: Build [x] [y] {[Worker]...[Worker]}");
			break;
		case "stealth":
		case "infiltrate":
			if (vector.length == 3) {
				Bee bufbee = curHive.getBee(vector[1]);
				Hive bufhive = curHive.getHostile(vector[2]);
				if (bufbee != null) {
					if (bufhive != null) {
						if (bufbee instanceof Worker) {
							if (bufbee.isAvailable()) {
								int stolen=0;
								if (Math.random() * 3 >= 1.0) {
									System.out.println(bufbee
											+ " tries to steal honey");
									if (tryInfiltrate(bufbee, bufhive, 1.5)) {
										stolen=bufhive.stealHoney(((Worker) bufbee).getCarry());
										if(stolen>0){
											System.out.println(bufbee+" stole "+stolen+ " honey");
											curHive.addHoney(stolen);
										} else System.out.println(bufhive.getName()+" has no honey stored");
									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								} else {
									System.out.println(bufbee
											+ " tries to steal wax");
									if (tryInfiltrate(bufbee, bufhive, 1.6)) {
										stolen=bufhive.stealWax(((Worker) bufbee).getCarry());
										if(stolen>0){
											System.out.println(bufbee+" stole "+stolen+ " wax");
											curHive.addWax(stolen);
										} else System.out.println(bufhive.getName()+" has no wax stored");
									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								}
								// steal stuff
								bufbee.decreaseAvailable();

							} else
								System.out
										.println(bufbee + " is not available");
						}
						else if (bufbee instanceof Drone) {
							if (bufbee.isAvailable()) {
								if (Math.random() * 3 >= 1.0) {
									System.out.println(bufbee
											+ " tries to gather information");
									if (tryInfiltrate(bufbee, bufhive, 0.6)) {
										Plant p = bufhive.getRandPlant();
										if(p==null){
											System.out.println(bufhive+" has absolutely no information stored");
											bufbee.decreaseAvailable();
											return;
										}
										if (curHive.getPlant(p.toString()) == null) {
											System.out
													.println(bufbee
															+ " gathered information about the plant "
															+ p);
											curHive.findPlant(p);
										} else {
											System.out
													.println(bufbee
															+ " gathered information about "
															+ p
															+ ", but you already know this plant");
										}

									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								} else {
									System.out.println(bufbee
											+ " tries to sabotage hostile combs");
									if (tryInfiltrate(bufbee, bufhive, 1.4)) {
										System.out
												.println(bufbee
														+ " entered the combs silently");
										printarray(bufhive.getCombstats());
										int sab = bufhive.sabotage();
										if (sab > 1) {
											System.out
													.println(bufbee
															+ " successfully destroyed "
															+ sab
															+ " levels of combs");
											printarray(bufhive.getCombstats());
										} else if (sab==1) {
											System.out
													.println(bufbee
															+ " successfully destroyed one comb");
											printarray(bufhive.getCombstats());
										} else System.out.println(bufbee+ " couldn't manage to destroy a comb");
										
									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								}
								// find hostile flower or sabotage combs
								bufbee.decreaseAvailable();
							} else
								System.out
										.println(bufbee + " is not available");
						}
						else if (bufbee instanceof Queen) {
							if (bufbee.isAvailable()) {
								if (Math.random() * 3 >= 1.0 || day<12 || bufbee.getAttack()<10) {//vor tag 12 wird nix ermordet
									System.out.println(bufbee
											+ " tries to steal a larva");
									if (tryInfiltrate(bufbee, bufhive, 1.0)) {
										Larva buflarva = bufhive.getRandLarva();
										if(buflarva!=null){
										System.out
												.println(bufbee
														+ " successfully stole the larva "
														+ buflarva);
										curHive.integrateLarva(buflarva);
										} else System.out.println(bufhive+" has no larvae");
									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								} else {
									System.out.println(bufbee
											+ " tries to kill a bee");
									if (tryInfiltrate(bufbee, bufhive, 1.4)) {
										System.out
												.println("Assassination successfull!");
										killRandomBee(bufhive
												.getAllBeesNoQueen());
										bufhive.clearGraveyard();

									} else {
										bufbee.die();
										curHive.clearGraveyard();
									}
								}
								// assasinate bees or steal a larva
								bufbee.decreaseAvailable();
							} else
								System.out
										.println(bufbee + " is not available");
						} else
							System.out
									.println(bufbee
											+ " is a larva and cannot infiltrate anything");
					} else
						System.out
								.println(vector[2] + " is not a hostile hive");
				} else
					System.out.println("404: Bee not found");
			} else
				System.out
						.println("Correct use of \"infiltrate\" is: infiltrate [bee] [hive]");
			break;
		case "aliasset":
			if(vector.length>2){
				params=cmd.split(" ",3);
				String key=params[1].toLowerCase();
				String[] value=params[2].split(";");
				for(String s:value){
					s=s.trim();
				}
				curHive.setAlias(key,value);
			} else System.out.println("Setting an alias is pretty complicated, see \"help alias\"");
			break;
		case "alias":
			if(vector.length==2){
				String[] bufarray = curHive.getAlias(vector[1]);
				if(bufarray!=null){
					runAlias(bufarray);
				} else System.out.println("There is no Alias called "+vector[1]);
			}else System.out.println("Correct use of \"alias\" is: alias [alias]");
			break;
		case "list":
		case "ls":
			if (vector.length == 2) {
				switch (vector[1].toLowerCase()) {
				case "bee":
				case "bees":
					printarray(curHive.listBees());
					break;
				case "plant":
				case "plants":
					printarray(curHive.listPlants());
					break;
				case "alias":
				case "aliases":
					printarray(curHive.getAllAliases());
					break;
				case "hungry":
					printarray(curHive.getAllHungry());
					break;
				case "hive":
				case "hives":
					printarray(curHive.listHives());
					break;
				case "idle":
				case "available":
					printarray(curHive.listIdle());
					break;
				default:
					System.out.println("Cannot list " + vector[1]);
					break;
				}

			} 
			else if(vector.length==3){
				switch(vector[1].toLowerCase()){
					case "alias":
					case "aliases":
						String[] bufarray=curHive.getAlias(vector[2].toLowerCase());
						if(bufarray!=null){
							System.out.println(vector[2]+": ");
							int i = 0;
							while (i<bufarray.length){
								System.out.print(bufarray[i]);
								i++;
								if(i<bufarray.length){
									System.out.print(" ; ");
								}
							}
							System.out.println("");
						} else System.out.println(vector[2]+" is not a valid alias");
						break;
					default:
						System.out
						.println("Correct use of \"list\" is: list bee/idle/plant/hive");
						break;
				}
			}
			else {
				System.out
				.println("Correct use of \"list\" is: list bee/idle/plant/hive");
			}
			break;
		case "cheattest": // das is ein cheat zum testen
			curHive.cheat();
			break;
		case "craft":
			if (vector.length == 3) {
				Worker workbuf = curHive.getWorker(vector[1]);
				if (workbuf != null) {
					if (vector[2].toLowerCase().equals("honey")) {
						workbuf.craftHoney();
					} else if (vector[2].toLowerCase().equals("wax")) {
						workbuf.craftWax();
					} else
						System.out
								.println("Third argument must be honey or wax");
				} else
					System.out
							.println("There is no Worker called " + vector[1]);
			} else
				System.out
						.println("Correct use of \"craft\" is: craft [Bee] honey/wax");
			break;
		case "advance":
		case "upgrade":
		case "train":
			if (vector.length == 3) {
				switch (vector[2]) {
				case "carry":
				case "harvest":
					curHive.advCarry(vector[1]);
					break;
				case "craft":
					curHive.advCraft(vector[1]);
					break;
				case "attack":
				case "engage":
				case "damage":
					curHive.advAttack(vector[1]);
					break;
				case "endurance":
				case "hunger":
				case "saturation":
					curHive.advHunger(vector[1]);
					break;
				case "brood":
				case "breed":
					curHive.advBrood(vector[1]);
					break;
				case "spy":
				case "infiltrate":
				case "stealth":
					curHive.advStealth(vector[1]);
					break;
				default:
					System.out
							.println("Only carry/craft/attack/endurance/stealth/brood are traits");
				}
			} else
				System.out
						.println("Correct use of \"advance\" is: advance [Bee] [trait]");
			break;
		case "eat":
			if (vector.length == 2) {
				Bee bufbee = curHive.getBee(vector[1]);
				if (bufbee != null) {
					if (!(bufbee instanceof Larva)) {
						if (bufbee.isAvailable()) {
							bufbee.eat();
							bufbee.decreaseAvailable();
						} else
							System.out.println(vector[1] + " is not available");
					} else
						System.out.println(vector[1]
								+ " is a larva and eats every day anyways");
				} else
					System.out.println("There is no bee called " + vector[1]);
			} else
				System.out.println("Correct use of \"eat\" is: eat [Bee]");

			break;
		case "commands":
		case "command":
			String[] narray = { "breed", "rename", "advance", "list", "status",
					"nextday", "smartnext", "craft", "harvest", "build",
					"scout", "spy", "infiltrate", "engage", "move", "alias","aliasset"};
			printarray(narray);
			break;
		default:
			if(vector.length==1 && curHive.hasAlias(vector[0])){
				String[] bufarray = curHive.getAlias(vector[0]);
				if(bufarray!=null){
					runAlias(bufarray);
				} else System.out.println("There is no Alias called "+vector[0]);
			}
			else System.out.println("command not found");
			break;
		}
	}

	public String readline() {
		// while(!scanner.hasNext()){
		// }
		return scanner.nextLine().trim();
	}

	public void printarray(String[] a) {
		for (String n : a) {
			System.out.println(n);
		}
	}

	public void battle(LinkedList<Bee> attackers, LinkedList<Bee> defenders,
			Hive defhive) {
		while (defhive.getQueen() != null && !(attackers.isEmpty())) // solange
																		// es
																		// was
																		// zum
																		// k�mpfen
																		// gibt
		{
			System.out.println("");
			System.out.println(defhive.getName() + " does "
					+ defhive.getDefense() + " damage against your Batallion");
			System.out.println("Your Batallion does " + getAttack(attackers)
					+ " damage against " + defhive.getName());
			if ((Math.random() * (getAttack(attackers) + defhive.getDefense())) < defhive
					.getDefense())// ausrechnen wer einen Verlust erleidet
			{
				// Fall: Attacker Stirbt
				if (killRandomBee(attackers)) {
					return; // wenn ne königin Stirbt
				}
			} else {
				if (killRandomBee(defenders)) {
					return;
				}
			}
			curHive.clearGraveyard();
			defhive.clearGraveyard();
		}
	}

	public boolean killRandomBee(LinkedList<Bee> bees) {
		// returns true if a queen got killed
		int rand = (int) (Math.random() * bees.size());
		Bee doomedbee = bees.get(rand);
		bees.remove(rand);

		doomedbee.die();
		if (doomedbee instanceof Queen) {
			System.out.println(doomedbee + " has been defeated!");
			return true;
		} else {
			System.out.println(doomedbee + " has been killed in action");
			return false;
		}

	}

	public int getAttack(LinkedList<Bee> bees) {
		int att = 0;
		for (Bee b : bees) {
			att = att + b.getAttack();
		}
		return att;
	}

	// Save und Load sind aus dem internet... mehr oder weniger. Ein bisschen
	// was hab ich schon selber gemacht
	// Naja, Informatiker sind nunmal moderne J�ger und Sammler...
	// Erschlagt mich nicht wegen dem Typecast, normalerweise caste ich nur bei
	// int zu float oder andersrum, war so im tutorial...
	public void save(String path) {
		// Write to disk with FileOutputStream
		FileOutputStream f_out;
		try {

			f_out = new FileOutputStream(Wikimanager.workingdir()+"Saves" + java.io.File.separatorChar
					+ path + ".data");

			// Write object with ObjectOutputStream

			ObjectOutputStream obj_out;

			obj_out = new ObjectOutputStream(f_out);

			obj_out.writeObject(new GameStatus(playerList, curPlayerIndex,
					curHive, day, playerFinished, hivenumber));
			// problem liegt beim saven von Hives
			obj_out.close();
		} catch (IOException e) {
			System.out.println("An error occured while serializing and saving "
					+ path + ".data");
		}

	}

	public void load(String path) {
		// Read from disk using FileInputStream
		GameStatus loaded = null;
		try {

			FileInputStream f_in = new FileInputStream(Wikimanager.workingdir()+"Saves"
					+ java.io.File.separatorChar + path + ".data");

			// Read object using ObjectInputStream
			ObjectInputStream obj_in = new ObjectInputStream(f_in);

			// Read an object

			Object obj = obj_in.readObject();

			loaded = (GameStatus) obj;

			// if (obj instanceof Vector)
			// {
			// Cast object to a Vector
			// Vector vec = (Vector) obj;

			// Do something with vector....
			// loaded=(GameStatus)vec;
			// }
			obj_in.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out
					.println(path
							+ " is not a valid savegame, restart the game and enter a valid save");
			System.exit(0);
		}

		curHive = loaded.getCurHive();
		curPlayerIndex = loaded.getCurPlayerIndex();
		day = loaded.getDay();
		hivenumber = loaded.getHivenumber();
		playerFinished = loaded.getPlayerFinished();
		playerList = loaded.getPlayerList();
	}

	public boolean tryInfiltrate(Bee infiltrator, Hive enemyhive,
			double detectchance) {
		LinkedList<Bee> defenders = enemyhive.getAllBees();
		int infilpower = infiltrator.getStealthlevel() * 100;
		double rand;
		for (Bee b : defenders) {
			rand = Math.random() * infilpower;
			if (b instanceof Queen) {
				if (b.getStealthlevel() * 3 * detectchance >= rand) {
					System.out.println(infiltrator
							+ " was caught and executed by " + b);
					return false;
				}
			}
			if (b instanceof Drone) {
				if (b.getStealthlevel() * 2 * detectchance >= rand) {
					System.out.println(infiltrator
							+ " was caught and executed by " + b);
					return false;
				}
			}
			if (b instanceof Worker) {
				if (b.getStealthlevel() * detectchance >= rand) {
					System.out.println(infiltrator
							+ " was caught and executed by " + b);
					return false;
				}
			}
		}

		return true;
	}
	
	public void runAlias(String[] cmds){
		recursiveRuns++;
		if(recursiveRuns>20){
			System.out.println("Your Alias creates an infinite loop. You should change that or it will never work correctly!");
			return;
		}
		for(String s:cmds){
			System.out.println("A>"+s.trim());
			System.out.println("- - - - - - - - - - - - - - -");
			interpretCommand(s.trim());
		}
		recursiveRuns=0;
	}

}
