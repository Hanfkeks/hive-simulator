public class Worker extends Bee implements java.io.Serializable{

	private int carrylevel;
	private int craftlevel;

	public Worker(String n, Hive h) {
		super(n, h);
		saturation = 6 + hungerlevel;
		carrylevel = 1;
		craftlevel = 1;
	}

	@Override
	public void update() {
		decrementSaturation();
		available=1;
	}
	
	public int getCarry(){
		return carrylevel*2+1;
	}
	
	public void eat(){
		if (myHive.consumePollen(1)) {
			saturation = 5 + hungerlevel + 2;
		} else {
			System.out.println(this+" has nothing to eat");
		}
	}
	
	public void craftHoney() {
		if (available > 0) {
			if (myHive.consumePollen(2 * craftlevel)) {
				myHive.addHoney(craftlevel);
				System.out.println(name + " just crafted " + craftlevel
						+ " Honey out of " + (2 * craftlevel) + " Pollen");
				available--;
			} else
				System.out.println(name + " needs at least " + (craftlevel * 2)
						+ " Pollen");
		}
		else System.out.println(name+" is not available");
	}

	public void craftWax() {
		if (available > 0) {
			if (myHive.consumePollen(3 * craftlevel)) {
				myHive.addWax(craftlevel);
				System.out.println(name + " just crafted " + craftlevel
						+ " Wax out of " + (3 * craftlevel) + " Pollen");
				available--;
			} else
				System.out.println(name + " needs at least " + (craftlevel * 3)
						+ " Pollen");
		}
		else System.out.println(name+" is not available");
	}

	@Override
	public void decrementSaturation() {
		saturation--;
		if (saturation == 0) {

			if (myHive.consumePollen(1)) {
				saturation = 5 + hungerlevel;
			} else {
				System.out.println("A Worker has starved to death");
				die();
			}
		}

	}

	public String[] giveDetails() {
		String [] bufstring = new String[9];
		bufstring[0]="Name: " + name;
		bufstring[1]="Attack-Level: " + attacklevel;
		bufstring[2]="Attack: " + getAttack();
		bufstring[3]="Endurance-Level: " + hungerlevel;
		bufstring[4]="Days until hunger: " + saturation;
		bufstring[5]="Carry-Level: " + carrylevel;
		bufstring[6]="Craft-Level: " + craftlevel;
		bufstring[7]="Stealth-Level: "+stealthlevel;
		bufstring[8]=availablestring();
		return bufstring;
	}
	
	@Override
	public void die() {
		myHive.killbee(this);

	}

	@Override
	public int getAttack() {
		return attacklevel;
	}
	
	public int getUpgradecostCraft(){
		return (int) (Math.pow(2, craftlevel-1));
	}
	
	public void advanceCraft(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostCraft())){
				craftlevel++;
				available--;
				System.out.println(name+" now crafts more Honey or Wax at once");
			}
			else System.out.println(name+" needs "+getUpgradecostCraft()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}
	
	public int getUpgradecostCarry(){
		return (int) (Math.pow(2, carrylevel-1));
	}
	
	public void advanceCarry(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostCarry())){
				carrylevel++;
				available--;
				System.out.println(name+" can now harvest more Pollen at once");
			}
			else System.out.println(name+" needs "+getUpgradecostCarry()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}

}
