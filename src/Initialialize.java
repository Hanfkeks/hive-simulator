import java.io.File;
import java.util.Scanner;

public class Initialialize {

	public static void main(String[] args) {
		int plcount=0;
		System.out.println("Hive Simulator");
		System.out.println("Type L to load a game, type N to start a new game");
		
		Scanner scanner = new Scanner(System.in);
		String bufline=scanner.nextLine();
		boolean exit=false;
		while(!exit){
		if(bufline.toLowerCase().equals("l")){
			File folder=new File(Wikimanager.workingdir()+"Saves");
			for(File f:folder.listFiles()){
				if(f.toString().toLowerCase().endsWith(".data")){
		            System.out.println(f.getName().substring(0, f.getName().length()-5));
				}
			}
			System.out.println("Enter the name of your savegame:");
			//evtl alles saves auflisten
			System.out.print(":>");
			bufline=scanner.nextLine();
			CommandPromtManager cmd=new CommandPromtManager(bufline,scanner);
			exit=true;
		} else if(bufline.toLowerCase().equals("n")){
		
		System.out.println("Ammount of Players:");
	    while (!scanner.hasNextInt()) {
	    	System.out.println("Please enter any number to set the ammount of players:");
	    	scanner.nextLine();
	        
	    }
	    plcount=scanner.nextInt();
	    System.out.println("Playercount: " + plcount);
	    
	    

		CommandPromtManager cmd=new CommandPromtManager(plcount,scanner);
		exit=true;
		
		}
		System.out.println("If you want to start a new game type in N, if you want to continue a savegame, type L");
		bufline=scanner.nextLine();
	}
		try {
			scanner.close();
		} catch (Exception e) {
		}
	}
}
