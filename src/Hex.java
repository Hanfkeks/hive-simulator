
public class Hex implements java.io.Serializable{
	private int level;
	private Hive myHive;
	
	public Hex(Hive h){
		level=0;
		myHive=h;
	}


	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getUpgradeCost(){
		return (int) Math.pow(2,level);		//Upgrade Kosten verdoppeln sich
	}
	
	public void upgrade(){
		if(myHive.consumeWax(getUpgradeCost())){
			level++;
			System.out.println("Upgrade Complete!");
		}
		else{
			System.out.println("You'll need " + getUpgradeCost() + " Wax to build the next level");
		}
		myHive.calculateStorage();
	}
	
	public int getStorageCap(){
		return level*3;			//damit man nicht unendlich lange in einem 3x3 hive bleiben kann...
	}
	
	public String toString(){
		String bufstring="";
		if(level<10){
			bufstring="0"+level+" ";
		}
		else{
			bufstring=level+" ";
		}
		return bufstring;
	}
}
