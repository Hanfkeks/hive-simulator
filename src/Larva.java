
public class Larva extends Bee implements java.io.Serializable{
	
	private int hatchtime;

	public Larva(String n, Hive h) {
		super(n, h);
		saturation=1;
		hatchtime=5;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		hatchtime--;
		if(hatchtime<=0){
			hatch();
		}else{
			decrementSaturation();
		}
		available=0;
		
	}
	public void eat(){
		if (myHive.consumeHoney(1)) {
			saturation = 1;
		} else {
			System.out.println(this+" has nothing to eat");
		}
	}
	public void hatch(){
		int r = (int) (Math.random()*6);
		if(r>1){
			System.out.println(name+" has hatched and became a Worker");
			myHive.hatchWorker(new Worker(name,myHive));
		}
		else{
			System.out.println(name+" has hatched and became a Drone");
			myHive.hatchDrone(new Drone(name,myHive));
		}
		myHive.killbee(this);
	}
	
	@Override
	public void decrementSaturation() {
		saturation--;
		if(saturation==0){
			
			if(myHive.consumeHoney(1)){
				saturation=1;
			}
			else{
				System.out.println("A Larva has dried out");
				die();
			}
		}
		
	}

	@Override
	public void die() {
		myHive.killbee(this);
	}

	@Override
	public int getAttack() {
		return 0;	//Larvas don't have attack
	}
	
	public String[] giveDetails() {
		String [] bufstring = new String[3];
		bufstring[0]="Name: " + name;
		bufstring[1]="Will consume 1 honey per day until hatched";
		bufstring[2]="Days until hatch: " + hatchtime;
		return bufstring;
	}

}
