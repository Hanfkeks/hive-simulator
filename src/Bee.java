public abstract class Bee implements java.io.Serializable{
	protected String name;
	protected int attacklevel;
	protected int hungerlevel;
	protected int stealthlevel;
	protected int available;
	protected int saturation;
	protected Hive myHive;

	// Construct0r:

	public Bee(String n, Hive h) {
		setName(n);
		setAvailable(1);
		setHungerlevel(1);
		setAttacklevel(1);
		stealthlevel=1;
		myHive = h;
	}

	// Meth:

	public abstract void update(); // jeden tag wird zb der hunger dekrementiert
									// etc.

	public abstract void decrementSaturation();
	public abstract String[] giveDetails();
	public abstract void eat();
	public abstract void die();	//Sterbevorgang is extrem komplex damit ich mir nicht selber das brett auf dem ich steh wegzieh.
								//Wenn die Biene n�mlich innerhalb einer iteration gel�scht wird, geht die iteration der linked list
								//nichtmehr weiter... weil ja der Listenknoten auch weg is. Deshalb der komplizierte schiess mit graveyard und so...
	public void delete(){
		myHive.delbee(this);
	}
	// Getters&Setters
	public abstract int getAttack();

	// Following are same for all Bees:
	public int getHungerlevel() {
		return hungerlevel;
	}
	
	public int getStealthlevel(){
		return stealthlevel;
	}
	
	public void setHive(Hive h){
		myHive=h;
	}

	public void setHungerlevel(int hungerlevel) {
		this.hungerlevel = hungerlevel;
	}
	
	public Hive getHive(){
		return myHive;
	}
	
	public void advanceStealth(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostStealth())){
				stealthlevel++;
				available--;
				System.out.println(name+" is now more likely to detect intruders and is better at infiltrating hostile hives");
			}
			else System.out.println(name+" needs "+getUpgradecostStealth()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}

	public int getAttacklevel() {
		return attacklevel;
	}

	public void setAttacklevel(int attacklevel) {
		this.attacklevel = attacklevel;
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		this.name = n;
	}

	public int getAvailable() {
		return available;
	}

	public void decreaseAvailable() {
		available--;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getSaturation() {
		return saturation;
	}

	public void setSaturation(int saturation) {
		this.saturation = saturation;
	}

	public String toString() {
		return name;
	}
	
	public String availablestring(){
		if(available>0){
			return "Available !";
		}
		else return "not available";
	}
	
	
	public int getUpgradecostAttack(){
		return (int) (Math.pow(2, attacklevel-1));
	}
	
	public int getUpgradecostStealth(){
		return (int) (Math.pow(2, stealthlevel-1));
	}
	
	public void advanceAttack(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostAttack())){
				attacklevel++;
				available--;
				System.out.println(name+" now does more damage");
			}
			else System.out.println(name+" needs "+getUpgradecostAttack()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}
	
	public int getUpgradecostHunger(){
		return (int) (Math.pow(2, hungerlevel-1));
	}
	
	public void advanceHunger(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostHunger())){
				hungerlevel++;
				available--;
				System.out.println(name+" now needs to eat less frequently");
			}
			else System.out.println(name+" needs "+getUpgradecostHunger()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}
	
	public boolean isAvailable(){
		if(available>0){
			return true;
		}
		else return false;
	}
}
