public class Queen extends Bee implements java.io.Serializable{

	public Queen(String n, Hive h) {
		super(n, h);
		saturation = 5 + hungerlevel;
	}

	@Override
	public int getAttack() {
		return (attacklevel * 3) + 5;
	}

	@Override
	public void update() {
		decrementSaturation();
		available=1;

	}
	
	public void eat(){
		if (myHive.consumeHoney(1)) {
			saturation = 4 + hungerlevel + 2;
		} else {
			System.out.println(this+" has nothing to eat");
		}
	}

	public void decrementSaturation() {
		saturation--;
		if (saturation == 0) {

			if (myHive.consumeHoney(1)) {
				saturation = 4 + hungerlevel;
			} else {
				System.out.println("Your Queen has starved to death");
				die();
			}
		}
	}

	public void die() {
		System.out.println(name
				+ " has died. Your hive will desintegrate within one day");
		myHive.killbee(this);
	}
	
	public String[] giveDetails() {
		String [] bufstring = new String[7];
		bufstring[0]="Name: " + name;
		bufstring[1]="Attack-Level: " + attacklevel;
		bufstring[2]="Attack: " + getAttack();
		bufstring[3]="Endurance-Level: " + hungerlevel;
		bufstring[4]="Days until hunger: " + saturation;
		bufstring[5]="Stealth-Level: "+stealthlevel;
		bufstring[6]=availablestring();
		return bufstring;
	}

}
