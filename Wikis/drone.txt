Wikipage of Drone

Your Drones are mainly used for defense, offense and reproduction.
A Drone will eat one honey every 3 + [endurance-level] days.
Since your Drone starts with a endurance-level of 1, this means it eats 1 honey every 4 days.

Your Drone does 3 damage plus 2*[attack-level].
The initial attack-level is 1. So your damage dealt will increase by 2 per attack-level.

A Drone can detect intruders 2 times better than a worker.
Advancing your stealth-level will increase it even more. (see: "help advance")

When infiltrating a Hive, your Drone can steal information about plants, or sabortage a comb. (see: "help infiltrate")

A Drone can breed Larvae. One per brood-level. (see: "help breed")

If you want information about possible new hives or enemy Hives, just use the "spy" command. (see: "help spy")