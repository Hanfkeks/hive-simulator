public class Drone extends Bee implements java.io.Serializable{

	private int breedlevel;

	public Drone(String n, Hive h) {
		super(n, h);
		saturation = 3 + hungerlevel;
		setBreedlevel(1);
	}

	@Override
	public void update() {
		decrementSaturation();
		available=1;
	}

	@Override
	public void decrementSaturation() {
		saturation--;
		if (saturation == 0) {

			if (myHive.consumeHoney(1)) {
				saturation = 3 + hungerlevel;
			} else {
				System.out.println("A Drone has starved to death");
				die();
			}
		}
	}
	
	public void eat(){
		if (myHive.consumeHoney(1)) {
			saturation = 3 + hungerlevel + 2;
		} else {
			System.out.println(this+" has nothing to eat");
		}
	}
	
	@Override
	public void die() {
		myHive.killbee(this);

	}

	@Override
	public int getAttack() {
		return (attacklevel * 2) + 2;
	}

	public int getBreedlevel() {
		return breedlevel;
	}

	public void setBreedlevel(int breedlevel) {
		this.breedlevel = breedlevel;
	}

	@Override
	public String[] giveDetails() {
		String [] bufstring = new String[8];
		bufstring[0]="Name: " + name;
		bufstring[1]="Attack-Level: " + attacklevel;
		bufstring[2]="Attack: " + getAttack();
		bufstring[3]="Endurance-Level: " + hungerlevel;
		bufstring[4]="Days until hunger: " + saturation;
		bufstring[5]="Breed-Level: " + breedlevel;
		bufstring[6]="Stealth-Level: "+stealthlevel;
		bufstring[7]=availablestring();
		return bufstring;
	}
	
	public int getUpgradecostBreed(){
		return (int) (Math.pow(2, breedlevel-1));
	}
	
	public void advanceBreed(){
		if(isAvailable()){
			if(myHive.consumeHoney(getUpgradecostBreed())){
				breedlevel++;
				available--;
				System.out.println(name+" now spawns more larvae when reproducing");
			}
			else System.out.println(name+" needs "+getUpgradecostBreed()+" to advance to the next Level");
		}
		else System.out.println(name+ " is not available");
	}
	
}
