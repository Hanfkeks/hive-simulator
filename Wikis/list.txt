Wikipage of List
Usage: "list bee","list plant","list hive","list idle","list alias","list hungry"
Also possible: "list bees","list plants","list hives","list available", "list aliases"
     or just type "ls" instead of "list"

List just creates a list of stuff you have:
 list bee:    Lists all of your bees and tells you what type of bee they are
 list plant:  Lists all the plants you have discovered
 list idle:   Lists all the bees that are still available
 list hive:   Lists all the hives and possible new homes that you've discovered
 list alias:  Lists all the aliases you've created
 list hungry: Lists all bees that will consume ressources at the beginning of the next day

Special:
 list alias [alias]: Shows you the structure of your alias, so you can just copy&paste it into your command promt
                         to change it