Wikipage of Bee

Since you are controlling bees in this game, every of your units is a bee.
There are 3 types of bees and larvae.
The Queen is the first type of bee. When your Queen dies, you loose and your hive will desintegrate.
Your Drones make up the second type of bees. They are mainly used for defense and reproduction.
Breeding (see: "help breed"), will spawn larvae wich will eventually develop into workers or drones.
Finally there are workers wich can harvest (see "help harvest") pollen from plants and craft (see "help craft") wax or honey.