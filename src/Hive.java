import java.util.HashMap;
import java.util.LinkedList;

public class Hive implements java.io.Serializable {

	// Attributes:

	private int maxSupply; // Ressourcen:
	private int supply;

	private int honey;
	private int pollen;
	private int wax;
	private int maxRessources;

	private Hex[][] combs; // Waben

	private Queen queen;
	private LinkedList<Plant> plantlist;
	private LinkedList<Worker> workerlist;
	private LinkedList<Drone> dronelist;
	private LinkedList<Larva> larvalist;
	private LinkedList<Hive> hivelist;
	private LinkedList<Bee> graveyard; // gestorbene bienen m�ssen noch kurz
										// referenziert werden
	private HashMap<String, String[]> aliases;
	private String hivename;

	// Construct0rs:

	public Hive(int combsX, int combsY, String queenname) { // only used for
															// Spawn
		queen = new Queen(queenname, this);
		hivename = queenname + "'s_Hive";
		combs = new Hex[combsX][combsY];
		int i = 0;
		int j = 0;
		while (j < combs.length) {
			i = 0;
			while (i < combs[j].length) {
				combs[j][i] = new Hex(this);
				i++;
			}
			j++;
		}
		honey = 4;
		wax = 0;
		pollen = 5;
		workerlist = new LinkedList<Worker>();
		dronelist = new LinkedList<Drone>();
		larvalist = new LinkedList<Larva>();
		graveyard = new LinkedList<Bee>();
		plantlist = new LinkedList<Plant>();
		hivelist = new LinkedList<Hive>();
		aliases = new HashMap<String, String[]>();
		dronelist.add(new Drone("DronePrime", this));
		workerlist.add(new Worker("FirstWorker", this));
		workerlist.add(new Worker("SecondWorker", this));
		workerlist.add(new Worker("ThirdWorker", this));
		calculateSupply();
		calculateStorage();
	}

	public Hive(int combsX, int combsY, int nummeration) {
		combs = new Hex[combsX][combsY];
		queen = null;
		int i = 0;
		int j = 0;
		while (j < combs.length) {
			i = 0;
			while (i < combs[j].length) {
				combs[j][i] = new Hex(this);
				i++;
			}
			j++;
		}
		workerlist = new LinkedList<Worker>();
		dronelist = new LinkedList<Drone>();
		larvalist = new LinkedList<Larva>();
		graveyard = new LinkedList<Bee>();
		plantlist = new LinkedList<Plant>();
		hivelist = new LinkedList<Hive>();
		aliases = new HashMap<String, String[]>();
		honey = 0;
		wax = 0;
		pollen = 0;
		hivename = "Possible_Hive_" + nummeration;
	}

	// Meth:

	public String getName() {
		return hivename;
	}

	public int nextHiveLimits() {// konnte mir keinen besseren namen ausdenken.
									// Generiert gr��ere limits f�r einen
									// neuen
									// Hive
		return (int) (((combs.length + combs[1].length) / 2) + Math.random()
				* combs.length);// die Hives sollten so ziemlich exponentiell
								// wachsen von der gr��e
	}

	public void discoverHive(Hive h) {
		hivelist.add(h);
	}

	public boolean alreadyDiscovered(Hive h) {
		if (hivelist.contains(h)) {
			return true;
		} else
			return false;
	}

	public void findPlant(Plant p) {
		plantlist.add(p);
	}

	public void harvest(Worker w, Plant p) {
		if (w.isAvailable()) {
			w.decreaseAvailable();
			addPollen(p.eatPollen(w.getCarry()));// wundersch�n
		} else
			System.out.println(w.getName() + " is not available");
	}

	public void advHunger(String s) {
		Bee b = getBee(s);
		if (b != null) {
			b.advanceHunger();
		} else
			System.out.println("There is no Bee called " + s);
	}

	public void advStealth(String s) {
		Bee b = getBee(s);
		if (b != null) {
			b.advanceStealth();
		} else
			System.out.println("There is no Bee called " + s);
	}

	public void advAttack(String s) {
		Bee b = getBee(s);
		if (b != null) {
			b.advanceAttack();
		} else
			System.out.println("There is no Bee called " + s);
	}

	public void advCarry(String s) {
		Worker b = getWorker(s);
		if (b != null) {
			b.advanceCarry();
		} else
			System.out.println("There is no Worker called " + s);
	}

	public void advCraft(String s) {
		Worker b = getWorker(s);
		if (b != null) {
			b.advanceCraft();
		} else
			System.out.println("There is no Worker called " + s);
	}

	public void advBrood(String s) {
		Drone b = getDrone(s);
		if (b != null) {
			b.advanceBreed();
		} else
			System.out.println("There is no Drone called " + s);
	}

	public String[] listBees() {
		String[] bufstring = new String[workerlist.size() + dronelist.size()
				+ larvalist.size() + 1];
		bufstring[0] = "Queen: " + queen;
		int i = 1;
		for (Worker w : workerlist) {
			bufstring[i] = "Worker: " + w;
			i++;
		}
		for (Larva l : larvalist) {
			bufstring[i] = "Larva: " + l;
			i++;
		}
		for (Drone d : dronelist) {
			bufstring[i] = "Drone: " + d;
			i++;
		}
		return bufstring;
	}

	public String[] listPlants() {
		String[] bufstring = new String[plantlist.size()];
		int i = 0;
		for (Plant w : plantlist) {
			bufstring[i] = "Plant: " + w;
			i++;
		}
		return bufstring;
	}

	public void hatchWorker(Worker w) {
		workerlist.add(w);
	}

	public void hatchDrone(Drone w) {
		dronelist.add(w);
	}

	public boolean hasAvailableWorker() {
		for (Worker x : workerlist) {
			if (x.isAvailable()) {
				System.out.println(x + " is still available");
				return true;
			}
		}
		return false;
	}

	public void buildCombs(int x, int y, Worker[] workarray) {
		if (x < combs.length && x >= 0 && y < combs[x].length && y >= 0) { // if
																			// inbounds
			if (workarray.length == (combs[x][y].getLevel() + 1)) {
				for (Worker w : workarray) {
					if (!w.isAvailable()) {
						System.out.println(w
								+ " is not available. Building failed;");
						return;
					}
				}
				if (combs[x][y].getUpgradeCost() <= wax) {
					for (Worker w : workarray) {
						w.decreaseAvailable();
					}
				}
				combs[x][y].upgrade();

			} else {
				System.out.println("You need exactly "
						+ (combs[x][y].getLevel() + 1) + " bees to upgrade "
						+ (x + 1) + ";" + (y + 1));
			}
		} else
			System.out
					.println("Your Hive does not contain "
							+ (x + 1)
							+ ";"
							+ (y + 1)
							+ ". Note that you shouldnt use Index access, first field is 1;1");
	}

	public void breedDrone(String d) {
		if (supply < maxSupply) {
			if (consumeHoney(1)) {
				Drone buf = getDrone(d.toLowerCase());
				if (buf != null) {
					if (buf.getAvailable() > 0) {
						buf.decreaseAvailable();
						int i = 0;
						while (i < buf.getBreedlevel()) {
							if (supply < maxSupply) {
								if (buf.getBreedlevel() > 1) {
									larvalist.add(new Larva(buf.getName()
											+ "'s_Infant_No" + (i + 1), this));
									calculateSupply();
								} else {
									larvalist.add(new Larva(buf.getName()
											+ "'s_Infant", this));
								}
							} else {
								System.out.println("There is no more space, "
										+ buf + " could only spawn " + i
										+ " Larvae");
							}
							i++;
						}
						System.out.println("Your drone sucessfully reproduced");
					} else {
						System.out.println("Your drone is not available");
					}
				} else {
					System.out.println("404: Drone not found");
				}
			} else {
				System.out
						.println("A drone needs honey if you want it to reproduce with your Queen");
			}
		} else
			System.out.println("There is not enough space for a Larva");
		calculateSupply();
	}

	public void cheat() {
		honey = 999;
	}

	public void calculateSupply() {
		setSupply(larvalist.size() + dronelist.size() + workerlist.size());
	}

	public void calculateStorage() {
		int i = 0;
		int j = 0;
		int counter = 10; // basis f�r jeden Hive ist eine Lagerkapazit�t
							// von 10
		while (j < combs.length) {
			i = 0;
			while (i < combs[j].length) {
				counter = counter + combs[j][i].getStorageCap();
				i++;
			}
			j++;
		}
		maxRessources = counter;
		counter = 0;
		j = 0;
		while (j < combs.length) {
			i = 0;
			while (i < combs[j].length) {
				counter = counter + (combs[j][i].getLevel() * 2);
				i++;
			}
			j++;
		}
		setMaxSupply(counter);
	}

	public String[] listIdle() {
		int i = 1;
		for (Worker w : workerlist) {
			if (w.isAvailable()) {
				i++;
			}
		}

		for (Drone d : dronelist) {
			if (d.isAvailable()) {
				i++;
			}
		}
		String[] bufarray = new String[i];
		bufarray[0] = "Idle Bees:";
		i = 1;
		for (Worker w : workerlist) {
			if (w.isAvailable()) {
				bufarray[i] = "Worker: " + w;
				i++;
			}
		}

		for (Drone d : dronelist) {
			if (d.isAvailable()) {
				bufarray[i] = "Drone: " + d;
				i++;
			}
		}
		return bufarray;
	}

	public boolean consumePollen(int x) {
		if ((pollen - x) >= 0) {
			pollen = pollen - x;
			return true;
		} else {
			System.out.println("Insufficient Pollen");
			return false;
		}
	}

	public boolean consumeHoney(int x) {
		if ((honey - x) >= 0) {
			honey = honey - x;
			return true;
		} else {
			System.out.println("Insufficient Honey");
			return false;
		}
	}

	public boolean consumeWax(int x) {
		if ((wax - x) >= 0) {
			wax = wax - x;
			return true;
		} else {
			System.out.println("Insufficient Wax");
			return false;
		}
	}

	public void addPollen(int x) {
		if ((pollen + x) < maxRessources) {
			pollen = pollen + x;
		} else {
			pollen = maxRessources;
			System.out
					.println("No more Pollen can be stored. You'll need aditional combs");
		}
	}

	public void addHoney(int x) {
		if ((honey + x) < maxRessources) {
			honey = honey + x;
		} else {
			honey = maxRessources;
			System.out
					.println("No more Honey can be stored. You'll need aditional combs");
		}
	}

	public void addWax(int x) {
		if ((wax + x) < maxRessources) {
			wax = wax + x;
		} else {
			wax = maxRessources;
			System.out
					.println("No more Wax can be stored. Build aditional combs!");
		}
	}

	public void renameBee(String original, String newname) {
		if (queen.getName().toLowerCase().equals(original.toLowerCase())) {
			queen.setName(newname);
			return;
		}
		for (Bee b : larvalist) {
			if (b.getName().toLowerCase().equals(original.toLowerCase())) {
				b.setName(newname);
				return; // bewusster abbruch, damit keine andere Biene renamed
						// wird.
						// ansonsten sind gleichnamige bienen nicht
						// selektierbar.
			}
		}
		for (Bee b : dronelist) {
			if (b.getName().toLowerCase().equals(original.toLowerCase())) {
				b.setName(newname);
				return;
			}
		}
		for (Bee b : workerlist) {
			if (b.getName().toLowerCase().equals(original.toLowerCase())) {
				b.setName(newname);
				return;
			}
		}
		System.out.println("There is no Bee called " + original);
	}

	public String[] listHives() {
		int i = 0;
		String[] bufarray = new String[hivelist.size()];
		for (Hive h : hivelist) {
			bufarray[i] = h.toString();
			i++;
		}
		return bufarray;
	}

	public String toString() {
		return hivename;
	}

	// Getters&Setters&Unimportant Methods:

	public Drone getDrone(String s) {
		for (Drone d : dronelist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		return null;
	}

	public Plant getPlant(String s) {
		for (Plant d : plantlist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		return null;
	}

	public Worker getWorker(String s) {
		for (Worker d : workerlist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		return null;
	}

	public Larva getLarva(String s) {
		for (Larva d : larvalist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		return null;
	}

	public Bee getBee(String s) {
		if (queen.toString().toLowerCase().equals(s.toLowerCase())) {
			return queen;
		}
		for (Larva d : larvalist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		for (Worker d : workerlist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		for (Drone d : dronelist) {
			if (d.getName().toLowerCase().equals(s.toLowerCase())) {
				return d;
			}
		}
		return null;

	}

	public int getMaxSupply() {
		return maxSupply;
	}

	public void setMaxSupply(int maxSupply) {
		this.maxSupply = maxSupply;
	}

	public int getSupply() {
		return supply;
	}

	public Hive getHostile(String h) {
		for (Hive x : hivelist) {
			if (x.toString().toLowerCase().equals(h.toLowerCase())
					&& x.queen != null) {
				return x;
			}
		}
		return null;
	}

	public void moveToHive(Hive h) {
		h.hivename = hivename;
		hivename = queen + "'s_Old_Hive";
		h.setQueen(queen);
		queen = null;
		h.dronelist = dronelist;
		dronelist = new LinkedList<Drone>();
		h.calculateStorage();
		h.calculateSupply();
		h.addHoney(honey);
		honey = 0;
		h.addPollen(pollen);
		pollen = 0;
		h.addWax(wax);
		wax = 0;
		h.plantlist = plantlist;
		plantlist = new LinkedList<Plant>();
		h.workerlist = workerlist;
		workerlist = new LinkedList<Worker>();
		h.hivelist = hivelist;
		h.hivelist.add(this);
		h.hivelist.remove(h);
		hivelist = new LinkedList<Hive>();
		h.aliases = aliases;
		aliases = new HashMap<String, String[]>();

		// kucken, dass die bienchen auch bescheid wissen
		h.queen.setHive(h);
		for (Bee b : h.workerlist) {
			b.setHive(h);
		}
		for (Bee b : h.dronelist) {
			b.setHive(h);
		}
	}

	public boolean allAvailable() {
		if (queen.isAvailable() == false) {
			return false;
		}
		for (Worker w : workerlist) {
			if (w.isAvailable() == false) {
				return false;
			}
		}
		for (Drone d : dronelist) {
			if (d.isAvailable() == false) {
				return false;
			}
		}
		return true;
	}

	public boolean hasLarva() {
		if (larvalist.size() == 0) {
			return false;
		} else
			return true;
	}

	public Hive getEmptyHive(String h) {
		for (Hive x : hivelist) {
			if (x.toString().toLowerCase().equals(h.toLowerCase())
					&& x.queen == null) {
				return x;
			}
		}
		return null;
	}

	public LinkedList<Bee> getAllBees() {
		LinkedList<Bee> buflist = new LinkedList<Bee>();
		buflist.addAll(larvalist);
		buflist.addAll(workerlist);
		buflist.addAll(dronelist);
		buflist.add(queen);
		return buflist;
	}

	public LinkedList<Bee> getAllBeesNoQueen() {
		LinkedList<Bee> buflist = new LinkedList<Bee>();
		buflist.addAll(larvalist);
		buflist.addAll(workerlist);
		buflist.addAll(dronelist);
		return buflist;
	}

	public int stealHoney(int ammount) {
		if (ammount <= honey) {
			honey = honey - ammount;
			return ammount;
		} else {
			int buffer = ammount - honey;
			honey = 0;
			return buffer;
		}
	}

	public int stealWax(int ammount) {
		if (ammount <= wax) {
			wax = wax - ammount;
			return ammount;
		} else {
			int buffer = ammount - wax;
			wax = 0;
			return buffer;
		}
	}

	public void endday() {
		for (Plant p : plantlist) {
			p.makeUpdateable();
		}
	}

	public Plant getRandPlant() {
		if (plantlist.size() == 0) {
			return null;
		}
		int rand = (int) (Math.random() * plantlist.size());
		return plantlist.get(rand);
	}

	public Larva getRandLarva() {
		if (larvalist.size() == 0) {
			return null;
		}
		int rand = (int) (Math.random() * larvalist.size());
		return larvalist.get(rand);
	}

	public void integrateLarva(Larva l) {
		Hive bufhive = l.getHive();
		l.die();
		bufhive.clearGraveyard();
		larvalist.add(l);
		l.setHive(this);
	}

	public int sabotage() { // returns how many levels have been sabotaged
		int rand1 = (int) (Math.random() * combs.length);
		int rand2 = (int) (Math.random() * combs[0].length);
		Hex bufhex = combs[rand1][rand2];
		int curlvl = bufhex.getLevel();
		int rand3 = (int) (Math.random() * curlvl);
		bufhex.setLevel(rand3);
		return curlvl - bufhex.getLevel();
	}

	public void setSupply(int supply) {
		this.supply = supply;
	}

	public void setQueen(Queen q) {
		queen = q;
	}

	public void killbee(Bee b) {
		graveyard.add(b);
	}

	public void delbee(Bee b) {

		if (b == queen) {
			queen = null;
		} else if (b instanceof Drone) {
			if (dronelist.contains(b)) {
				dronelist.remove(b);
			}
		} else if (b instanceof Worker) {
			if (workerlist.contains(b)) {
				workerlist.remove(b);
			}
		} else if (b instanceof Larva) {
			if (larvalist.contains(b)) {
				larvalist.remove(b);
			}
		} else {// kann nicht passieren
			System.out
					.println("There is a glitch in this programm... The bee: <"
							+ b + "> cannot be removed");
		}
		// graveyard.add(b);
		calculateSupply();
	}

	public Queen getQueen() {
		return queen;
	}

	public int getDefense() {
		calculateStorage();
		int defcounter = 0;
		for (Drone d : dronelist) {
			defcounter = defcounter + d.getAttack();
		}
		for (Worker w : workerlist) {
			defcounter = defcounter + w.getAttack();
		}
		return (int) (maxSupply / 2) + queen.getAttack() + defcounter;
	}

	public String[] getAllHungry() {
		String[] bufarray;
		int counter = 0;
		for (Drone d : dronelist) {
			if (d.getSaturation() == 1) {
				counter++;
			}
		}
		for (Worker w : workerlist) {
			if (w.getSaturation() == 1) {
				counter++;
			}
		}
		if(queen.getSaturation()==1){
			counter++;
		}
		counter=counter+larvalist.size();
		if(counter==0){
			bufarray=new String[1];
			bufarray[0]="No bee will consume honey tomorrow morning";
			return bufarray;
		}
		bufarray=new String[counter];
		counter=0;
		for (Drone d : dronelist) {
			if (d.getSaturation() == 1) {
				bufarray[counter]=d.toString();
				counter++;
			}
		}
		for (Worker w : workerlist) {
			if (w.getSaturation() == 1) {
				bufarray[counter]=w.toString();
				counter++;
			}
		}
		if(queen.getSaturation()==1){
			bufarray[counter]=queen.toString();
			counter++;
		}
		for(Larva l:larvalist){
			bufarray[counter]=l.toString();
			counter++;
		}
		return bufarray;
	}

	public String[] getCombstats() {
		String[] bufarray = new String[combs.length + 2];
		bufarray[0] = "Combs:";
		int i = 0;
		int j = 0;
		while (j < combs.length) {
			i = 0;
			while (i < combs[j].length) {
				if (i == 0) {
					bufarray[j + 1] = "";
				}
				bufarray[j + 1] = bufarray[j + 1] + combs[j][i].toString();
				i++;
			}
			j++;
		}
		bufarray[bufarray.length - 1] = "";
		return bufarray;
	}

	public String[] getStats() {
		calculateStorage();
		calculateSupply();
		String[] bufarray = new String[8];
		bufarray[0] = "Supply: " + supply + "/" + maxSupply;
		bufarray[1] = "Pollen: " + pollen + "/" + maxRessources;
		bufarray[2] = "Honey: " + honey + "/" + maxRessources;
		bufarray[3] = "Wax: " + wax + "/" + maxRessources;
		bufarray[4] = "Drones: " + dronelist.size();
		bufarray[5] = "Workers: " + workerlist.size();
		bufarray[6] = "Larvae: " + larvalist.size();
		bufarray[7] = "Total Defense: " + getDefense();

		return bufarray;
	}

	public void updateEveryone() {
		// clean Graveyard
		queen.update();
		for (Bee b : larvalist) {
			b.update();
		}
		for (Bee b : dronelist) {
			b.update();
		}
		for (Bee b : workerlist) {
			b.update();
		}
		for (Plant p : plantlist) {
			p.update();
		}
		// gestorbene Bienen nun killn
		clearGraveyard();

	}

	public void clearGraveyard() {
		for (Bee b : graveyard) {
			b.delete();
		}
		graveyard.clear();
	}

	public void setAlias(String key, String[] value) {
		if (aliases.containsKey(key)) {
			System.out.println("Overwriting Alias " + key);
		}
		aliases.put(key.toLowerCase(), value);
	}

	public String[] getAlias(String key) {
		return aliases.get(key.toLowerCase());
	}

	public String[] getAllAliases() {
		String[] bufarray = new String[aliases.size()];
		int i = 0;
		for (String a : aliases.keySet()) {
			bufarray[i] = a;
			i++;
		}
		return bufarray;
	}

	public boolean hasAlias(String key) {
		return aliases.containsKey(key.toLowerCase());
	}

	public int getWax() {
		return wax;
	}

	public int getHoney() {
		return honey;
	}

	public int getPollen() {
		return pollen;
	}

	public int getMaxRessources() {
		return maxRessources;
	}
}
