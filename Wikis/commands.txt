Wikipage of Commands
Usage: "commands"
Also possible: "command"
You will recieve a list of commands you can give.
Don't be afraid to type in any command without a parameter, you will most likely get a message telling you how to use it.
There are only a few commands that work without parameters.
  "status": it will give you information about your combs and your hive
    see "help status" for more information and parameters you can use (optional)
  "exit": will close the game, without saving it.
    "quit" also does the same. There is no wikipage for it since it's pretty self-explainatory.
  "nextday": ends your turn. It will be your turn on the next day.
     your Bees will be available again when a day has passed.
  "smartnext": ends your turn if you don't have any available workers left.
Note that capitalisation does not matter when typing in the command.
"coMmANds" will do the job aswell.