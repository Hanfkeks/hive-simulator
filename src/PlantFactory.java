import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class PlantFactory {
	// Fancy Fancy Black Magic.... Pflanzt b�ume und son scheiss
	// musste fast die h�lfte nachkucken... schande schande
	final static Charset ENCODING = StandardCharsets.UTF_8;

	public static Plant growRandomPlant(){
		int regeneration=0;
	
		double regmodPre=1.0;
		double regmodSuf=1.0;
		
		int max=0;
		
		double maxmodPre=1.0;
		double maxmodSuf=1.0;
		
		int cur=0;
		
		String name="";
		
		String pre="";
		String suf="";
		
		int rand=0;
		char sepchar=java.io.File.separatorChar;
		String filepath=Wikimanager.workingdir()+"Ressources"+sepchar+"plantprefixes.txt";
		List<String> fixes;
		try{
			fixes = readTXT(filepath);
			rand=(int)(Math.random()*fixes.size());
			pre=fixes.get(rand);
			
		}catch(IOException e){
			System.out.println("WARNING: <"+filepath+"> could not be read. Fix it and restart the game");
			return new Plant(100,20,10,"Muthafucking_Default_Flower");
		}
		String[] vect = pre.split(" ");
		if(vect.length!=3){
			System.out.println("WARNING: <"+filepath+"> line "+rand+ " is formated incorrectly");
			return new Plant(250,100,1,"Muthafucking_Magnificent_Cactus");
		}
		
		try{
			regmodPre = Double.parseDouble(vect[1]);
			maxmodPre = Double.parseDouble(vect[2]);
			pre=vect[0];
		}catch(NumberFormatException e){
			System.out.println("WARNING: <"+filepath+"> line "+rand+ " has incorrectly formated Numbers");
			return new Plant(100,50,12,"Muthafucking_Burning_Tree");
		}
		
		//Now onto the Suffixes
		filepath=Wikimanager.workingdir()+"Ressources"+sepchar+"plantsuffixes.txt";
		try{
			fixes = readTXT(filepath);
			rand=(int)(Math.random()*fixes.size());
			suf=fixes.get(rand);
			
		}catch(IOException e){
			System.out.println("WARNING: <"+filepath+"> could not be read. Fix it and restart the game");
			return new Plant(100,20,10,"Muthafucking_Default_Flower");
		}
		vect = suf.split(" ");
		if(vect.length!=3){
			System.out.println("WARNING: <"+filepath+"> line "+rand+ " is formated incorrectly");
			return new Plant(250,100,1,"Muthafucking_Magnificent_Cactus");
		}

		try{
			regmodSuf = Double.parseDouble(vect[1]);
			maxmodSuf = Double.parseDouble(vect[2]);
			suf=vect[0];
		}catch(NumberFormatException e){
			System.out.println("WARNING: <"+filepath+"> line "+rand+ " has incorrectly formated Numbers");
			return new Plant(100,50,12,"Muthafucking_Burning_Tree");
		}
		max=(int)(((Math.random()*110)+20)*maxmodSuf*maxmodPre);
		regeneration=(int)(((Math.random()*10)+1)*regmodSuf*regmodPre);
		cur=(int) (max*Math.random());
		name=pre+"_"+suf;
		
		return new Plant(max,cur,regeneration,name);
	}
	
	public static List<String> readTXT(String aFileName) throws IOException {
	    Path path = Paths.get(aFileName);
	    return Files.readAllLines(path, ENCODING);
	  }
}
