Wikipage of Commands
Usage: "harvest [worker] [plant]"
Also possible: "carry"

A worker will harvest pollen from the plant. This will decrease the pollen from the plant (see "status plant [plant]").
If a plant has no pollen left there's nothing to be harvested.
If your depot is full, some pollen will be lost.
Note that a worker will harvest 1+(2*carrylevel) of pollen. This means every carrylevel makes your bee harvest 2 more pollen.