public class Plant implements java.io.Serializable {
	private String name;
	private int maxPollen;
	private int curPollen;
	private int regenPollen;
	private boolean alreadyUpdated;

	public Plant(int max, int cur, int regen, String n) {
		maxPollen = max;
		curPollen = cur;
		regenPollen = regen;
		name = n;
	}

	public void update() {
		if (!alreadyUpdated) {
			if (curPollen + regenPollen >= maxPollen) {
				curPollen = maxPollen;
			} else {
				curPollen = curPollen + regenPollen;
			}
			alreadyUpdated=true;
		}
	}
	
	public void makeUpdateable(){
		alreadyUpdated=false;
	}

	public String getName() {
		return name;
	}

	public int eatPollen(int x) {
		if (x > curPollen) {
			int buf = curPollen;
			System.out.println("Only " + buf + " Pollen have been harvested");
			curPollen = 0;
			return buf;
		} else {
			curPollen = curPollen - x;
			System.out.println(x + " Pollen have been harvested");
			return x;
		}
	}

	public String[] giveDetails() {
		String[] bufstring = new String[3];
		bufstring[0] = "Plantname: " + name;
		bufstring[1] = "Pollencount: " + curPollen + "/" + maxPollen;
		bufstring[2] = "Pollenregeneration: " + regenPollen + " Pollen per Day";

		return bufstring;
	}

	public String toString() {
		return name;
	}
}
