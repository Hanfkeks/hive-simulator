import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class Wikimanager {
	final static Charset ENCODING = StandardCharsets.UTF_8;

	public static String workingdir(){
		Wikimanager mng= new Wikimanager();
		String className = mng.getClass().getName().replace('.', '/');
		
		   String classJar =
			mng.getClass().getResource("/" + className + ".class").toString();
		   if (classJar.startsWith("jar:")) {
		     String vals[] = classJar.split("/");
		     for (String val: vals) {
		       if (val.contains("!")) {
		         return val.substring(0, val.length() - 1)+java.io.File.separatorChar;
		       }
		     }
		   }
		   String curdir=System.getProperty("user.dir");
		   if(curdir.endsWith(java.io.File.separatorChar+"bin")){
			   return ".."+java.io.File.separatorChar;
		   }
		   return "";
		
	}
	
	public static String[] loadWikipage(String page){
		String[] zeilen;
		char sepchar=java.io.File.separatorChar;
		String filepath=workingdir()+"Wikis"+sepchar+page.toLowerCase()+".txt";
		filepath=System.getProperty("user.dir")+sepchar+filepath;
		//System.out.println(filepath);
		List<String> seite;
		try{
			Path path = Paths.get(filepath);
		    seite = Files.readAllLines(path, ENCODING);
		    int i=0;
		    zeilen=new String[seite.size()];
			for(String s:seite){
				zeilen[i]=s;
				i++;
			}
			return zeilen;
		}catch(IOException e){
			zeilen=new String[1];
			zeilen[0]=page+" does not have a wikipage";
			return zeilen;
		}
		
	}
}
