Wikipage of List
Usage: "status","status hive","status combs","status honey","status pollen","status wax",
       "status bee [bee]","status plant [plant]"
Also possible: stats instead of status

Show detailed information:
   status:       Shows combs and hivestats
   status combs: Shows your combs and their levels
   status hive:  Shows hivestats. Ressources, Supply, Ammount of bees

   status [ressource]:   Shows the ammount of the ressource and the max. storage capability
   status bee [bee]:     Gives you detailed information about your bee
   status plant [plant]: Gives you detailed information about your plant